#!/bin/python

# :beginHead
# :author     			:zachary dimaria
# :support     			:zak@jammvisual.com
# :type     			:python
# :title     			:createFtrackSeqFromFlame
# :description			:builds seq from flame export
# :usage				:createFtrackSeqFromFlame.py jobPath seqName shotNames(nat010_nat020_nat030)
# :endHead

# Copyright (C) 2014 JAMM

import os
import subprocess
import sys
sys.path.insert(0, os.path.expandvars('$TOOLS/ftrackBase/latest/python'))
import ftrack

#parse args
jobPath = sys.argv[1] #'/mnt/storage/jobs/carlsnatural/library/preMasterGrade/'
jobName = jobPath.split('/jobs/')[1].split('/library/')[0]
seqName = sys.argv[2]
shotNames = sys.argv[3].split('_')

#get job and create seq
ft_job = ftrack.getProject(jobName)
ft_seq = ft_job.createSequence(seqName)

#grab current job structure version
f_jobStructVersion = open(os.path.expandvars('$ENGINEERING/jobStruct/jobStruct_version'), 'r')
jobStructVersion = f_jobStructVersion.readlines()[-1].replace('\n', '')
f_jobStructVersion.close()
print '#%s#' % jobStructVersion

#build paths
path_seqStruct = os.path.join(os.path.expandvars('$ENGINEERING/jobStruct'), jobStructVersion, 'SEQ')
path_shotStruct = os.path.join(os.path.expandvars('$ENGINEERING/jobStruct'), jobStructVersion, 'SHOT')
path_seq = os.path.join('/jobs', jobName, 'sequences', seqName)

print path_seqStruct
print path_shotStruct
print path_seq

#copy base seq structure
subprocess.call('cp -rvp %s %s' % (path_seqStruct, path_seq), shell=True)

for shotName in shotNames:
	# shotName = seqName + shotNumber
	ft_shot = ft_seq.createShot(shotName)
	path_shot = os.path.join(path_seq, shotName)
	
	#copy base shot structure
	subprocess.call('cp -rvp %s %s' % (path_shotStruct, path_shot), shell=True)
