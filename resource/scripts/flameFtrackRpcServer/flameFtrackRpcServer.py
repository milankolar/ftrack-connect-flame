# :beginHead
# :author               :zachary dimaria
# :support              :zak@jammvisual.com
# :type                 :python
# :title                :flameFtrackRpcServer
# :description          :listens for hooks running in flame
# :usage                :
# :endHead

# Copyright (C) 2014 JAMM

import sys
import subprocess
from SimpleXMLRPCServer import SimpleXMLRPCServer
import os

server = SimpleXMLRPCServer(('', 9001),allow_none=True)

def test():
	return True
server.register_function(test)

def createFtrackSeqFromFlame(jobPath, seqName, shotNames):
	jobName = jobPath.split('/')[2]
	command = 'python /software/tools/scripts/createFtrackSeqFromFlame.py %s %s %s' % (jobPath, seqName, shotNames)
	subprocess.call(command, shell=True)
server.register_function(createFtrackSeqFromFlame)


def createPlatesFromConform(jobName, seqName):
	command = 'source /software/tools/scripts/setJob %s; /software/tools/scripts/createPlatesFromConform %s %s' % (jobName, jobName, seqName)
server.register_function(createPlatesFromConform)


try:
	print 'ctrl c to exit'
	server.serve_forever()
except KeyboardInterrupt:
	print 'exiting'