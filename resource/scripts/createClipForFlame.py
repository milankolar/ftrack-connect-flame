#!/bin/python

# :beginHead
# :author               :zachary dimaria
# :support              :zak@jammvisual.com
# :type                 :python
# :title                :createClipForFlame
# :description          :builds builds and assetizes 3d plates from flame conform
# :usage                :createClipForFlame.py [elementfolderpath]
# :endHead

# Copyright (C) 2014 JAMM


import datetime
import OpenImageIO as oiio
import os
import shutil
import time
import sys
sys.path.insert(0, '%s/ftrackBase/latest/python'% os.getenv('TOOLS'))
import ftrack


def getVersion(ft_asset, versionNum):
    print 'getVersion:', ft_asset.getName(), versionNum
    for ft_version in ft_asset.getVersions():
        if ft_version.getVersion() == versionNum:
            return ft_version
    return None

xml_head = '''<?xml version="1.0" encoding="UTF-8"?>
<clip type="clip" version="3">
    <name type="string">"%(elementName)s"</name>
    <tracks type="tracks">
        <track type="track" uid="%(assetId)s">
            <trackType>video</trackType>
            <duration type="time">
                <rate type="rate">24</rate>
                <nbTicks>%(length)d</nbTicks>
            </duration>
            <name type="string">%(elementName)s</name>

            <feeds currentVersion="%(currentVersion)s">'''
xml_feed = '''
                <feed type="feed" vuid="%(version)s" uid="%(assetVersionId)s">
                    <storageFormat type="format">
                        <type>video</type>
                        <channelsDepth type="uint">%(bitDepth)d</channelsDepth>
                        <channelsEncoding type="string">%(bitType)s</channelsEncoding>
                        <channelsEndianess type="string">Little Endian</channelsEndianess>
                        <fieldDominance type="int">2</fieldDominance>
                        <height type="uint">%(resY)d</height>
                        <nbChannels type="uint">%(numChannels)d</nbChannels>
                        <pixelLayout type="string">%(pixelOrder)s</pixelLayout>
                        <pixelRatio type="float">%(pixelRatio)d</pixelRatio>
                        <rowOrdering type="string">down</rowOrdering>
                        <width type="uint">%(resX)d</width>
                    </storageFormat>
                    <userData type="dict">
                        <recordTimecode type="time" label="00:00:00+00">
                            <rate type="rate">24</rate>
                            <nbTicks>%(length)d</nbTicks>
                        </recordTimecode>
                    </userData>
                    <spans type="spans" version="3">
                        <span type="span" version="3">
                            <duration>%(length)d</duration>
                            <path>%(imagePath)s</path>
                        </span>
                    </spans>
                </feed>'''
xml_bridge = '''
            </feeds>
        </track>
    </tracks>
    <versions type="versions" currentVersion="%(currentVersion)s">'''
xml_version = '''
    <version type="version" uid="%(version)s">
        <name>%(version)s</name>
        <creationDate>%(dateTime)s</creationDate>
    </version>'''
xml_tail = '''
    </versions>
</clip>'''

elementPath = sys.argv[1]
os.chdir(elementPath)
##/jobs/rnd_pipeline/library/vfx/bid/bid000/renders/bid000_lgt_titanium/v037/master 

#parse path and get ftrack objects
jobName = elementPath.split('/')[2]
seqName = elementPath.split('/')[5]
shotName = elementPath.split('/')[6]
assetType = elementPath.split('/')[7]
if assetType == 'renders':
    assetType = 'img'
assetName = elementPath.split('/')[8]

print jobName, seqName, shotName, assetType, assetName

ft_shot = ftrack.getShot((jobName, seqName, shotName))
ft_asset = ft_shot.getAsset(assetName, assetType)

#renders are handled differntly than other images because they have split components.
#in path_element: we need a clip file for every componenet, which stores all the versions of that component.

if assetType == 'img':
    #collect component types
    versions = []
    components = []
    for version in sorted(os.listdir(elementPath)):
        if os.path.isdir(os.path.join(elementPath, version)):
            print 'is dir ', version
            versions.append(version)
            for component in os.listdir(os.path.join(elementPath, version)):
                components.append(component)   

    #create clip file for each component
    for component in components:
        clipName = '%s_%s' % (assetName, component)
        
        #check for and back up old clip
        if os.path.isfile('%s.clip' % clipName):
            bakClips = sorted([f for f in os.listdir(elementPath) if f[-9:] == '.clip.bak'])
            nextBakVersion = 'v%03d' % (int(bakClips[-1][-12:-9]) + 1)
            nextBakClip = '%s_%s.clip.bak' % (clipName, nextBakVersion)
        else:
            nextBakClip = '%s_v001.clip.bak' % clipName

        #find longest sequence
        maxLen = 0
        for version in versions:
            maxLen = max(maxLen, len(os.listdir(os.path.join(version, component))))

        print 'maxLen: ', maxLen

        #create new clip
        f_clip = open(nextBakClip, 'w')

        #write head
        head = {
            'elementName': '%s_%s' % (assetName, component),
            'assetId':ft_asset.getId(),
            'length':maxLen,
            'currentVersion':versions[-1]
        }
        f_clip.write(xml_head%head)

        #write feeds
        floats=[
            'HALF',
            'FLOAT',
            'DOUBLE'
        ]
        ints = [
            'INT',
            'UINT',
            'UINT8',
            'INT8',
            'UINT16',
            'INT16',
            'UINT32',
            'INT32',
            'UINT64',
            'INT64'
        ]

        for version in versions:
            componentPath = os.path.join(elementPath, version, component)
            print 'componentPath ', componentPath
            sampleImage = sorted(os.listdir(componentPath))[0]
            sampleImagePath = os.path.join(componentPath, sampleImage)
            imgIn = oiio.ImageInput.open(sampleImagePath)
            spec = imgIn.spec()
            extra = { attrib.name:attrib.value for attrib in spec.extra_attribs }
            if str(spec.format.basetype) in floats:
                bitType = 'Float'
            elif str(spec.format.basetype) in ints:
                bitType = 'Integer'
            else:
                bitType = 'ERROR!'

            startFrame = sampleImage.split('.')[-2]
            endFrame = sorted(os.listdir(componentPath))[-1].split('.')[-2]
            baseName = sampleImage.split(version)[0]
            print baseName, version, startFrame, endFrame
            imageName = '%s%s.[%s-%s].exr' % (baseName, version, startFrame, endFrame)
            imagePath = os.path.join(componentPath, imageName)
            print 'imageName: ', imageName
            print 'imagepath: ', imagePath

            versionNum = int(version[1:])
            ft_version = getVersion(ft_asset, versionNum)
            feed = {
                'version':version,
                'assetVersionId':ft_version.getId(),
                'bitDepth':(spec.channel_bytes()*8),
                'bitType':bitType,
                'resX':spec.width,
                'resY':spec.height,
                'numChannels':spec.nchannels,
                'pixelOrder':''.join(spec.channelnames),
                'pixelRatio':spec.get_float_attribute('PixelAspectRatio', 1),
                'length':maxLen,
                'imagePath':imagePath
            }
            f_clip.write(xml_feed%feed)


        #write bridge
        bridge = {
            'currentVersion':versions[-1]
        }
        f_clip.write(xml_bridge%bridge)

        #write versions
        for version in versions:
            sampleImagePath = os.path.join(version, os.listdir(version)[0])
            timeStamp = os.path.getmtime(sampleImagePath)
            versiond = {
                'version':version,
                'dateTime':str(datetime.datetime.fromtimestamp(timeStamp))
            }
            f_clip.write(xml_version%versiond)

        #write tail
        f_clip.write(xml_tail)

        #close clip
        f_clip.close()

        #copy clip to live clip
        shutil.copy2(nextBakClip, '%s_%s.clip' % (assetName, component))

#else do non split images the old way.
else:
    #check for and back up old clip
    if os.path.isfile('%s.clip' % assetName):
        bakClips = sorted([f for f in os.listdir(elementPath) if f[-9:] == '.clip.bak'])
        nextBakVersion = 'v%03d' % (int(bakClips[-1][-12:-9]) + 1)
        nextBakClip = '%s_%s.clip.bak' % (assetName, nextBakVersion) 
    else:
        nextBakClip = '%s_v001.clip.bak' % assetName

    #find longest sequence
    maxLen = 0
    for version in versions:
        maxLen = max(maxLen, len(os.listdir(version)))

    #create new clip
    f_clip = open(nextBakClip, 'w')

    #write head
    head = {
        'elementName':assetName,
        'assetId':ft_asset.getId(),
        'length':maxLen,
        'currentVersion':versions[-1]
    }
    f_clip.write(xml_head%head)

    #write feeds
    floats=[
        'HALF',
        'FLOAT',
        'DOUBLE'
    ]
    ints = [
        'INT',
        'UINT',
        'UINT8',
        'INT8',
        'UINT16',
        'INT16',
        'UINT32',
        'INT32',
        'UINT64',
        'INT64'
    ]

    for version in versions:
        sampleImage = os.listdir(version)[0]
        sampleImagePath = os.path.join(version, os.listdir(version)[0])
        imgIn = oiio.ImageInput.open(sampleImagePath)
        spec = imgIn.spec()
        extra = { attrib.name:attrib.value for attrib in spec.extra_attribs }
        if str(spec.format.basetype) in floats:
            bitType = 'Float'
        elif str(spec.format.basetype) in ints:
            bitType = 'Integer'
        else:
            bitType = 'ERROR!'

        startFrame = sampleImage.split('.')[-2]
        endFrame = os.listdir(version)[-1].split('.')[-2]
        extension = sampleImage.split('.')[-1]
        baseName = sampleImage.split(version)[0]
        imageName = '%s%s.[%s-%s].%s' % (baseName, version, startFrame, endFrame, extension)
        imagePath = os.path.join('/jobs', jobName, 'library/vfx', seqName, shotName, assetType, assetName, version, imageName)
        print 'imagePath: ', imagePath

        versionNum = int(version[1:])
        ft_version = getVersion(ft_asset, versionNum)
        feed = {
            'version':version,
            'assetVersionId':ft_version.getId(),
            'bitDepth':(spec.channel_bytes()*8),
            'bitType':bitType,
            'resX':spec.width,
            'resY':spec.height,
            'numChannels':spec.nchannels,
            'pixelOrder':''.join(spec.channelnames),
            'pixelRatio':spec.get_float_attribute('PixelAspectRatio', 1),
            'length':maxLen,
            'imagePath':imagePath
        }
        f_clip.write(xml_feed%feed)

    #write bridge
    bridge = {
        'currentVersion':versions[-1]
    }
    f_clip.write(xml_bridge%bridge)

    #write versions
    for version in versions:
        sampleImagePath = os.path.join(version, os.listdir(version)[0])
        timeStamp = os.path.getmtime(sampleImagePath)
        versiond = {
            'version':version,
            'dateTime':str(datetime.datetime.fromtimestamp(timeStamp))
        }
        f_clip.write(xml_version%versiond)

    #write tail
    f_clip.write(xml_tail)

    #close clip
    f_clip.close()

    #copy clip to live clip
    shutil.copy2(nextBakClip, '%s.clip' % assetName)

    #tell jake that there are new versions
    #import xmlrpclib
    #proxy = xmlrpclib.ServerProxy('http://flame01:9000')
    #proxy.sendFlameMessage('New Asset Version!\n\n%s'%assetName)
    print 'done.'