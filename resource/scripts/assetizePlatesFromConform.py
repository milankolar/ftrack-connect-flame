import ftrack
import os
import subprocess
import sys
import xmlrpclib

#jobName seqName shotName assetName assetType imagePath

#/jobs/carlsnatural/sequences/nat/nat230/images/plates/nat230_l1_lin/v001/nat230_l1_lin_v001.%04d.exr

#/jobs/carlsnatural/sequences/nat/nat230/images/plates/nat230_l1_rec/v001/nat230_l1_rec_v001.%04d.jpg

 
imagePath = sys.argv[1]
thumbPath = sys.argv[2]
print imagePath.split('/')

jobName = imagePath.split('/')[2]
seqName = imagePath.split('/')[4]
shotName = imagePath.split('/')[5]
assetName = imagePath.split('/')[9]
version = imagePath.split('/')[10]

grade = assetName.split('_')[-2]
space = assetName.split('_')[-1]

#check grade and space and set asset type
if grade == 'grd':
	if space == 'rec':
		assetType = 'backplate'
	else:
		assetType = 'graded'
#elif grade == 'flt':
else: #switch to flt after carlsnat!
	if space == 'rec':
		assetType = 'backplate'
	else:
		assetType = 'plates'


#print jobName, seqName, shotName, assetName

ft_shot = ftrack.getShot((jobName, seqName, shotName))
ft_shot.createThumbnail(thumbPath)
ft_asset = ft_shot.createAsset(assetName, assetType)
ft_assetVersion = ft_asset.createVersion()
ft_assetVersion.createComponent(file=imagePath)
ft_assetVersion.createThumbnail(thumbPath)
ft_assetVersion.publish()

#create post master paths and symlink
postVersionPath = os.path.join('/jobs', jobName, 'library/vfx', seqName, shotName, assetType, assetName, version)
print '#####postVersionPath:'
print '#####', postVersionPath
if not os.path.exists(postVersionPath):
	os.makedirs(postVersionPath, 0775)

pubVersionPath = os.path.join('/jobs', jobName, 'sequences', seqName, shotName, 'pub/images', assetType, assetName, version)
print '#####pubVersionPath:'
print '#####', pubVersionPath
for pubImage in os.listdir(pubVersionPath):
	print '#####pubImage: ', pubImage
	pubImagePath = os.path.join(pubVersionPath, pubImage)
	print '####pubImagePath: ', pubImagePath
	postImagePath = os.path.join(postVersionPath, pubImage)
	print '####postImagePath:', postImagePath
	subprocess.call('ln -s %s %s' % (pubImagePath, postImagePath), shell=True)


#ln to best and latest
#no longer linking images! only clip files
# for pubImage in os.listdir(pubVersionPath):
# 	pubImagePath = os.path.join(pubVersionPath, pubImage)
# 	bestImagePath = os.path.join(bestShotPath, pubImage)
# 	latestImagePath = os.path.join(latestShotPath, pubImage)
# 	subprocess.call('ln -s %s %s' % (pubImagePath, bestImagePath), shell=True)
# 	subprocess.call('ln -s %s %s' % (pubImagePath, latestImagePath), shell=True)

#mirror premaster structure in outsourcing roto and tracking
rotoVersionPath = os.path.join('/jobs', jobName, 'library/outsource/roto', seqName, shotName, version)
trackingVersionPath = os.path.join('/jobs', jobName, 'library/outsource/tracking', seqName, shotName, version)
if not os.path.exists(rotoVersionPath):
	os.makedirs(rotoVersionPath, 0775)
if not os.path.exists(trackingVersionPath):
	os.makedirs(trackingVersionPath, 0775)	

#create / update clip files in post master
#this is stupid and we should just import the module
postElementPath = os.path.join('/jobs', jobName, 'library/vfx', seqName, shotName, assetType, assetName)
tools = os.getenv('TOOLS')
subprocess.call('python %s/scripts/createClipForFlame.py %s' % (tools, postElementPath), shell=True)


#create best path if not created
bestShotPath = os.path.join('/jobs', jobName, 'library/vfx/best', seqName, shotName)
if not os.path.exists(bestShotPath):
	os.makedirs(bestShotPath, 0775)

#rm best and latest if exists
for bestImage in os.listdir(bestShotPath):
	bestImagePath = os.path.join(bestShotPath, bestImage)
	os.remove(bestImagePath)



#rm best clip if exists
clipName = '%s.clip' % assetName
bestClipPath = os.path.join(bestShotPath, clipName)

if os.path.exists(bestClipPath):
	os.remove(bestClipPath)

#ln clip to best
liveClipPath = os.path.join(postElementPath, bestClip)
subprocess.call('ln -s %s %s' % (liveClipPath, bestClipPath), shell=True)

# serverProxy = xmlrpclib.ServerProxy('http://flame01:9000')
# message = "There are new ftrac" 
# detail = 
# serverProxy.displayMessage(message, detail)