import os

def batchSetupSaved(setupPath):
	print '\n\nbegin batchSetupSaved'
	global batchSetupPath
	batchSetupPath = setupPath
	print batchSetupPath
	#/usr/discreet/project/CarlsNatural/batch/flame/nat090/nat090_comp_v01.batch
	print 'end batchSetupSaved'

def batchExportBegin(info, userData):
	print '\n\nbegin batchExportBegin'
	# for i in info:
	# 	print '%s : %s' % (str(i), str(info[i]))

	jobName = batchSetupPath.split('project/')[1].split('/batch')[0]
	seqName = userData['nodeName'].split('/')[1][:3]
	shotName = userData['nodeName'].split('/')[1]

	assetName = userData['exportPath']
	# precomp_grade
	# precomp_cleanup
	# comp
	if assetName in ['precomp_cleanup', 'precomp_cleanup']:
		assetType = 'precomp'
	else:
		assetType = 'comp'

	print 'jobName: ', jobName
	print 'seqName: ', seqName
	print 'shotName: ', shotName

	shotElementPath = os.path.join('/jobs', jobName, 'sequences', seqName, shotName, 'images', assetType, assetName)

	#find next version:
	if not os.path.exists(shotElementPath):
		version = 'v001'
	else:
		version = 'v%03d' % ( int(os.listdir(shotElementPath)[-1][1:]) + 1 )

	exportPath = os.path.join(shotElementPath, version)

	print 'end batchExportBegin'
	return True

def batchExportEnd(info, userData):
	print '\n\nbegin batchExportEnd'
	for i in info:
		print '%s : %s' % (str(i), str(info[i]))
	print 'end batchExportEnd'
