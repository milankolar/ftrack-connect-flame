import os
import sys
import subprocess
import xmlrpclib
print '\n\n\n'
print "found and running flame hooks"

proxy = xmlrpclib.ServerProxy('http://lic01:9000')


def useBackburnerPostExportAsset():
	return True

def postExportSequence(info, userData):
	print '\n\nrunning postExportSequence'
	print "opening subprocess to create seq on ftrack"
	global seqName
	seqName = str(info["sequenceName"])
	print '%s %s %s' % (str(info["destinationPath"]), str(info["sequenceName"]), '_'.join(info["shotNames"]))
	# command = 'sshpass -p "zak" ssh -o StrictHostKeyChecking=no zak@ws04.jamm python /software/tools/scripts/createFtrackSeqFromFlame.py %s %s %s' % (str(info["destinationPath"]), str(info["sequenceName"]), '_'.join(info["shotNames"]))
	# subprocess.call(command, shell=True)
	return proxy.createFtrackSeqFromFlame(str(info["destinationPath"]), str(info["sequenceName"]), '_'.join(info["shotNames"]))
	print "finished creating seq on ftrack!"
	print 'finished postExportSequence'

def postExportAsset(info, userData):
	print '\n\nrunning postExportAsset'
	for i in userData:
		print '%s : %s ' % (i, str(userData[i]))
	return True

def postExport(info, userData):
	print '\n\nrunning postExport'
	print 'info: ', str(info)
	print 'userData: ', str(userData)
	print 'opening subprocess to assetize plates'
	print 'pulling in global seqName as: ', seqName
	jobName = str(info['destinationPath']).split('/')[2]
	#seqName = str(info['destinationPath']).split('/')[3]
	# command = 'sshpass -p "zak" ssh -o StrictHostKeyChecking=no zak@ws04.jamm /software/tools/scripts/createPlatesFromConform %s %s' % (jobName, seqName)
	return proxy.createPlatesFromConform(jobName, seqName)
	print 'finished assetizing plates'
	print 'finished postExport'