# :beginHead
# :author               :zachary dimaria
# :support              :zak@jammvisual.com
# :type                 :python
# :title                :flameMessage.py
# :description          :message displayed to flame (initiated by xml server)
# :usage                :python flameMessage.py "main message" "detailed message"
# :endHead

# Copyright (C) 2014 JAMM

import sys
from PyQt4 import QtGui, QtCore

stylesheet = '''
QWidget {
    color: #CACACA;
    background-color: #444444;
}


QPushButton {
    background-color: #606060;
}

QComboBox {
    background-color: #606060;
}

QTableWidget {
    color: #CACACA;
    background-color: #2A2A2A;
    selection-background-color: #678EB2;
    gridline-color: #2A2A2A;
}

QLineEdit {
    background-color: #2A2A2A;
}

QListView {
    background-color: #2A2A2A;
}

QTreeView {
    background-color: #2A2A2A;
}

QTextEdit {
    background-color: #2A2A2A;
}


QHeaderView::section {
    background: #606060;
}

'''



class ClickLabel(QtGui.QLabel):
    def __init__(self, parent):
        QtGui.QLabel.__init__(self, parent)

    def mouseReleaseEvent(self, ev):
        self.emit(QtCore.SIGNAL('clicked()'))

class FlameMessage(QtGui.QWidget):
    def __init__(self, message, detail):
        QtGui.QWidget.__init__(self, None)

        self.message = message
        self.detail = detail

        self.initEnv()
        self.initUi()
   

    def initEnv(self):
        pass

    def initUi(self):  
        self.hbox_main = QtGui.QHBoxLayout()
        self.clab_icon = ClickLabel(self)
        self.clab_icon.setMinimumSize(1,1)
        self.gif_anim = QtGui.QMovie('alertGreen.gif', QtCore.QByteArray(), self)
        self.gif_anim.setCacheMode(QtGui.QMovie.CacheAll)
        self.gif_anim.setSpeed(300)

        self.clab_icon.setPixmap(QtGui.QPixmap('alertGreen.png'))

        # animation = QtCore.QPropertyAnimation(self.clab_icon, 'pixmap')
        # animation.setDuration(10000)
        # animation.setLoopCount(2)
        # animation.setStartValue(QtCore.QPixmap('alertGreen.png'))
        # animation.setEndValue(QtCore.QPixmap('alertGreenBright.png'))
        # animation.start()

        self.clab_icon.setMovie(self.gif_anim)
        self.gif_anim.start()
        self.clab_icon.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)

        self.connect(self.clab_icon, QtCore.SIGNAL('clicked()'), self.event_lab_icon_clicked)

        self.hbox_main.addWidget(self.clab_icon)
        self.hbox_main.setContentsMargins(0,0,0,0)
        self.setLayout(self.hbox_main)

        #build window, move to corner of screen and show
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.X11BypassWindowManagerHint )
        rect_screen = QtGui.QApplication.desktop().screen().rect()
        size = 30
        dualMon = 1

        self.setGeometry(
            rect_screen.right()*dualMon - size,
            rect_screen.bottom() - size,
            size,
            size
        )

        self.show()

    def event_lab_icon_clicked(self):
        self.showMessage()
        QtCore.QCoreApplication.instance().quit()

    def showMessage(self):
        message = QtGui.QMessageBox()
        message.setText(self.message)
        message.setDetailedText(self.detail)
        message.setStandardButtons(QtGui.QMessageBox.Ok)
        message.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.X11BypassWindowManagerHint )
        message.setStyleSheet(stylesheet)
        message.setWindowTitle('New ftrack Assets!')
        message.setIconPixmap(QtGui.QPixmap('ftrack_icon.png'))
        self.center(message)
        message.exec_()


    def center(self, widget):
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        widget.move(frameGm.topLeft())  

# app = QtGui.QApplication(sys.argv)
# if len(sys.argv) < 3:
#     message = "There is a new version of:\n\nbid000_comp"
#     detail = "bid000_comp v004 published by: Jonathan Vaughn"
# else:
#     message = sys.argv[1]
#     detail = sys.argv[2]
# flameMessage = FlameMessage(message, detail)
# # flameMessage.show()
# app.exec_()