# :beginHead
# :author               :zachary dimaria
# :support              :zak@jammvisual.com
# :type                 :python
# :title                :messageServer
# :description          :xmlrpc server to send ftrack messages to flame
# :usage                :messageServer
# :endHead

# Copyright (C) 2014 JAMM

import sys
from SimpleXMLRPCServer import SimpleXMLRPCServer
from PyQt4 import QtGui, QtCore
from flameMessage import FlameMessage
import os

server = SimpleXMLRPCServer(('', 9002))

class mymainwindow(QtGui.QMainWindow):
	def __init__(self):
	    QtGui.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)



def displayMessage(message, detail):
	app = QtGui.QApplication(sys.argv)
	flameMessage = FlameMessage(message, detail)
	app.exec_()

server.register_function(displayMessage)

try:
	print 'ctrl c to exit'
	server.serve_forever()
except KeyboardInterrupt:
	print 'Exiting'