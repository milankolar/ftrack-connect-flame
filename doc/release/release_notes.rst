..
    :copyright: Copyright (c) 2015 ftrack

.. _release/release_notes:

*************
Release notes
*************

.. release:: 0.1.0
    :date: 2015-01-28

    .. change:: new

        Initial release of the :term:`ftrack connect` extension for Flame.
