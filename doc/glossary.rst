..
    :copyright: Copyright (c) 2015 ftrack

********
Glossary
********

.. glossary::

    ftrack
        Cloud based Creative Project management tool for Production Tracking,
        Asset Management and Team Collaboration.

        .. seealso::

            `ftrack <https://ftrack.com>`_

    ftrack connect
        ftrack connect application, installed locally onto each individuals
        machine. It is used to connect ftrack with third party applications.

        .. seealso::

            :ref:`ftrack connect <ftrackconnect:about>`

    Extension
        An extension is a piece of software that can be added to an application
        to enhance its capabilities.
